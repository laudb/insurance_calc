const express    = require('express')
    , app        = express()
    , PORT       = process.env.PORT || 3000
    , logger     = require('morgan')
    , bodyParser = require('body-parser')
    , mongoose   = require('mongoose')
    , uri        = 'mongodb://localhost/insurance'
    , Car        = require('./Car.js').carModel
    , insurance  =  require('./insurance.js');


    mongoose.connect(uri);

    mongoose.connection.on('connected', function () {
      console.log('Successfully connected to ', uri);

      mongoose.connection.db.collection('cars').count(function (err, count) {

        if (count === 0) {
          let testCars = new Car([{
              insured: 'Kofi Essoun',
              address: 'Taifa, Accra',
              contact: '0302932452',
              make: 'Honda',
              cc: '',
              regNum: 'M-13 GR 1456',
              chasis: 'SRDFGHR5613FYUVH',
              engNum: 'DRFGFYUUK56',
              year: 2013,
              numSeats: 2,
              liy: 2015,
              si: 15000
          },{
              insured: 'xyz Company Ltd',
              address: 'Taifa, Accra',
              contact: '0302932453',
              make: 'Toyota Landcruiser',
              cc: 3200,
              regNum: 'GW 3214-15',
              chasis: 'WRDFGHS5643FSEZB',
              engNum: 'AIFGAYEUK63',
              year: 2012,
              numSeats: 10,
              liy: 2002,
              si: 220000
          }]);

          testCars.save();
        }
        else {
          console.log(count + " cars added");
        }

      });
    });

    mongoose.connection.on('error', function (err) {
      console.log('Error connecting database ', err);
    });



    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(logger('dev'))

    // default entry point
    app.get('/', (req, res) => {
      res.statusCode = 200;
      return res.json({message:'Welcome to Insurance Calculator'});
    });

    // POST endpoint
    app.post('/calculate_insurance', (req, res) => {

      // get query from request
      let carType = req.body.type;
      let carId = req.body.carId;

      console.log({carType, carId});

      // find car by chasis property;
      Car.findOne({ chasis: carId }).exec(function (err, data) {
        if (err) {
          res.statusCode = 500;
          res.json({"Error": 'Error finding Car', err});
        }
        if (!data) {
          res.statusCode = 404;
          res.json({ response :'Car not found in database'});
        }

        console.log('calculating data');
        // console.log(insurance)
        let result = insurance.totalThirdPartyPremium(data);
        console.log(result)
        res.statusCode = 200;
        res.json({ "type":carType, "insurance_price": result, "car_id": carId });
      });
    });

    // run application at this location
    app.listen( PORT , () => console.log('listening to app at ', PORT ) );

    module.exports = app
