// validate carId
const validateCarId = function (req, res, next) {
  let carId = req.body.carId;
  if ( carId.length === 14 && carId.substring(0,3) === 'SRD' ) {
    // return success
    statusCode = 200;
    next()
  } else {
    // return error
    statusCode = 400;
    res.json('Invalid Car Id');
  }
}

// validate carType
const  validateCarType = function (req, res, next) {
  let carType = req.body.carType;
      carType = carType.toLowerCase();

  if (carType !== 'comprehensive' || 'third-party' ) {
    statusCode = 400;
    res.json('Invalid Car Type');
  }
  next()
}
