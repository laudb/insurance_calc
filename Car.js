const mongoose = require('mongoose');

const carSchema = new mongoose.Schema({
    insured: String,
    address: String,
    contact: String,
    make: String,
    cc: Number,
    regNum: String,
    chasis: String,
    engNum: String,
    year: Number,
    numSeats: Number,
    liy: Number,
    si: Number
});

exports.carModel = mongoose.model('Car', carSchema);
