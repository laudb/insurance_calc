const express   = require('express')
    , app       = require('../index.js')
    , mocha     = require('mocha')
    , chai      = require('chai')
    , expect    = require('chai').expect
    , request   = require('supertest')
    , url    = 'http://localhost:3000/'
    , mongoose   = require('mongoose')
    , uri        = 'mongodb://localhost/insurance'
    , Car        = require('../Car.js').carModel;



describe('GET / endoint', function () {
  it('should return a message', function (done) {

    const message = 'Welcome to Insurance Calculator';

    request(app)
      .get('/')
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(function(res) {
        res.body.message === message;
      })
      .end(done);
  });
});

describe('POST /calculate_insurance', function () {

  it('should make a query with third-party:id ', function (done) {

    let data = {"type":"third-party","carId":"SRDFGHR5613FYUVH"};

    request(app)
      .post('/calculate_insurance')
      .set('Accept', 'application/json')
      .send(data)
      .expect('Content-Type', /json/)
      .expect(function (res) {
        res.body.type === "third-party";
        res.body.car_id === "SRDFGHR5613FYUVH";
      })
      .end(done);
  });
});
